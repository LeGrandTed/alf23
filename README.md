# Projet ALF23
 Mené par Luca Faubourg dans le cadre d'un projet universitaire sur stm32F411 nucleo-64 map.

## Materiels utilisé

  * Carte STM32F411 nucléo-64
  * Driver moteur Cytron MDD10A
  * Capteur infrarouge SHARP 2D120x
  * Capteur Ultrason HC-SR04
  * 2 moteurs electriques
  * Chassis et boite de vitesse de chez tamiya
 
## Connection des éléments

|Materiel         |  PIN  |
|-----------------|-------|
|PWM moteur 1     |   PB6 |
|Rotation moteur 1|   PB4 |
|PWM moteur 2     |   PB7 |
|Rotation moteur 1|   PB5 |
|Data sharp 2D120x|   PA1 |
|Echo HC-SR04     |   PA0 |
|Trigger HC-SR04  |   PA12|

## Alimentation
 La carte est alimentée par le port usb de l'ordinateur

 Les moteurs sont alimentés en 7 Volts et 1 Ampère
