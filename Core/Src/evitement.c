/*
 * evitement.c
 *
 *  Created on: Mar 26, 2023
 *      Author: Luca Faubourg
 */
#include "evitement.h"
uint32_t passage = 0;


void evitement(int * evasion,TIM_HandleTypeDef *htim,GPIO_TypeDef * port,uint16_t pin_rotate1,uint16_t pin_rotate2)
{
	switch(passage) // Algorithme d'évitement pour contourner un objet part la droite
	{
	case 0: // Tourne vers la droite
		HAL_GPIO_WritePin(port, pin_rotate1, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(port, pin_rotate2, GPIO_PIN_SET);
		passage ++;
		break;
	case 3: // Tourne vers la gauche
		HAL_GPIO_WritePin(port, pin_rotate1, GPIO_PIN_SET);
		HAL_GPIO_WritePin(port, pin_rotate2, GPIO_PIN_RESET);
		passage ++;
		break;
	case 6: // Tourne vers la gauche
		HAL_GPIO_WritePin(port, pin_rotate1, GPIO_PIN_SET);
		HAL_GPIO_WritePin(port, pin_rotate2, GPIO_PIN_RESET);
		passage ++;
		break;
	case 9: // Tourne vers la droite
		HAL_GPIO_WritePin(port, pin_rotate1, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(port, pin_rotate2, GPIO_PIN_SET);
		passage ++;
		break;
	case 10: // Avance tout droit et remet à 0 l'évitement une fois la trajectoire initiale reprise
		HAL_GPIO_WritePin(port, pin_rotate1, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(port, pin_rotate2, GPIO_PIN_RESET);
		passage = 0;
		*evasion = 0;
		break;
	default: // Avance tout droit
		HAL_GPIO_WritePin(port, pin_rotate1, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(port, pin_rotate2, GPIO_PIN_RESET);
		passage ++;
		break;
	}
}

