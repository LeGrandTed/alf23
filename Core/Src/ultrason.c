/*
 * ultrason.c
 *
 *  Created on: Mar 26, 2023
 *      Author: Luca Faubourg
 */
#include "ultrason.h"
uint32_t rising = 1;
double timerStart = 0;
double timerStop = 0;

// Donne la distance mesurée par le capteur ultrason
void get_distance(uint32_t * dist, TIM_HandleTypeDef *htim){
	if(rising) // Premiere capture quand le signale de retour est détecté
	{
		timerStart = HAL_TIM_ReadCapturedValue(htim,TIM_CHANNEL_1);
		rising = 0;
	}
	else // Première capture quand le signale de retour est fini
	{
		timerStop = HAL_TIM_ReadCapturedValue(htim,TIM_CHANNEL_1);
		rising = 1;

		if(timerStart > timerStop) // Si la première capture et la seconde ne sont pas sur le même cycle
		{
			timerStop = timerStop + 4294967295; // Comble l'écart dû au changement de cycle
		}
		*dist = (timerStop-timerStart)*0.034; 	// Différence entre les captures pour avoir le temps
												// multiplication par la vitesse du son en cm/µs
		*dist = *dist >> 1; // division par 2 à cause de l'aller-retour
	}

}

