/*
 * infrarouge.c
 *
 *  Created on: Mar 26, 2023
 *      Author: Luca Faubourg
 */
#include "infrarouge.h"

uint32_t get_value(ADC_HandleTypeDef * hadc)
{
	return HAL_ADC_GetValue(hadc); // Lis la tension renvoyée par l'intensité lumineuse du capteur infrarouge
}
