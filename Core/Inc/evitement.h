/*
 * evitement.h
 *
 *  Created on: Mar 26, 2023
 *      Author: Luca Faubourg
 */

#ifndef INC_EVITEMENT_H_
#define INC_EVITEMENT_H_

#include "stm32f4xx_hal.h"

void evitement(int * tour,TIM_HandleTypeDef *htim,GPIO_TypeDef * port,uint16_t pin_rotate1,uint16_t pin_rotate2);

#endif /* INC_EVITEMENT_H_ */
