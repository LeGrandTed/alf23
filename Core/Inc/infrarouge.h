/*
 * infrarouge.h
 *
 *  Created on: Mar 26, 2023
 *      Author: Luca Faubourg
 */

#ifndef INC_INFRAROUGE_H_
#define INC_INFRAROUGE_H_

#include "stm32f4xx_hal.h"

uint32_t get_value(ADC_HandleTypeDef* hadc);

#endif /* INC_INFRAROUGE_H_ */
