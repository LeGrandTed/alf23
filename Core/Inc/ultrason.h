/*
 * ultrason.h
 *
 *  Created on: Mar 26, 2023
 *      Author: ted
 */

#ifndef INC_ULTRASON_H_
#define INC_ULTRASON_H_

#include "stm32f4xx_hal.h"

void get_distance(uint32_t * distance, TIM_HandleTypeDef *htim);

#endif /* INC_ULTRASON_H_ */
